#!/bin/sh

if test "x$DEBEMAIL" = "x"
then
    echo "please set DEBEMAIL environment variable!" 1>&2
    exit 1
fi

SRCPACK=$(dpkg-parsechangelog -SSource)
VERSION=$(dpkg-parsechangelog -SVersion)
git tag -s -u "$DEBEMAIL" -m "$SRCPACK release $VERSION" "$SRCPACK/$VERSION"

