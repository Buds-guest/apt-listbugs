
The Debian package **apt-listbugs**
===================================


Introduction
------------

`apt-listbugs` is a tool which retrieves bug reports from the Debian
Bug Tracking System and lists them. In particular, it is intended to
be invoked before each installation or upgrade by APT, or other similar
package managers, in order to check whether the installation/upgrade is safe.

Most Debian developers, and some users, use the unstable version of
Debian, because this version is the latest snapshot which includes many
new features and packages.

APT is a great and convenient tool to let your system easily track the
latest package versions from unstable.
On the other hand, this capability can also make your system
fragile. If a broken package is uploaded to Debian unstable,
the package will soon get installed on many Debian systems.

The same reasoning holds for the testing version of Debian, even
though to a smaller extent.

`apt-listbugs` can help by warning its users about bugs in packages
which are about to be installed/upgraded and by giving the opportunity
to avoid or defer an unsafe installation/upgrade.

You can easily install **apt-listbugs** by using APT. Usually, no customization
is required. However, the use of a proxy server is recommended, in order
to reduce the number of accesses to the BTS.


Notes
-----

This version of `apt-listbugs` uses the Debian BTS SOAP interface for
bug retrieval.

`apt-listbugs` has a simple built-in interactive viewer. It uses the
`querybts` program as a back-end. To enable this feature, you need to
install the **reportbug** package. In addition, you can select broken
packages for pinning, to avoid automatically upgrading them. However,
pinning is not effective immediately, and requires restarting your APT
session.

The pinning will be removed by a cron.daily job (or by an equivalent
systemd .timer unit), when the bugs no longer affect the candidate version
of the package.

If you install a web browser package (which provides **www-browser**), you
can view bug lists in HTML.
`sensible-browser` from the **sensible-utils** package or `xdg-open` from
the **xdg-utils** package are also usable to select a web browser among
the installed ones.


Need for a controlling terminal
-------------------------------

`apt-listbugs` requires a controlling terminal for user interaction.
It will default to non-interactive auto-pin mode (equivalent to
options `-qnF`), if its standard output is not a tty.


Current Design Limitation
-------------------------

There is a problem with packages which have source-version and
binary-version mismatch. The BTS tracks the source version, while
`apt-listbugs` tracks the binary version. This conflict results in
version tracking not functioning for such packages. A popular example
of such a package is **gcc**.

